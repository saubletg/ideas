import React from 'react'
import Image from 'next/image'

function Banner() {
  return (
    <div className='flex items-center bg-yellow-400 border-y border-black py-10'>
        <div className='px-10 space-y-5'>
            <h1 className='text-6xl max-w-xl font-serif'>
                <span className='underline decoration-black decoration-4'>Ideas</span> 
                {" "}
                is a place to write, read an connect
            </h1>
            <h2>
                It's easy and free to post your thinking on any topic and connect with millions of readers.
            </h2>
        </div>

        <span className='hidden md:flex h-52 w-44'>
            
        </span>
    </div>
  )
}

export default Banner